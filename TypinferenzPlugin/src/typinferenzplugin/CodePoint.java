package typinferenzplugin;

import org.antlr.v4.runtime.Token;

public class CodePoint {

	public final Token offset;

	public CodePoint(Token offset){
		this.offset = offset;
	}

	public int getPositionInCode() {
		return offset.getStartIndex();
	}

	public boolean equals(Object obj){
		if(!(obj instanceof CodePoint))return false;
		CodePoint p = (CodePoint) obj;
		if(p.getPositionInCode()!=this.getPositionInCode())return false;
		return true;
	}
}
