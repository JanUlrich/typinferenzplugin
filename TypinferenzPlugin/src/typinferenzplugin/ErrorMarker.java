package typinferenzplugin;

import java.util.Vector;

import org.eclipse.core.resources.IMarker;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.source.Annotation;

import typinferenzplugin.editor.JavEditor;

public class ErrorMarker extends JavMarker{

	private String message;
	private CodePoint point;
	
	public ErrorMarker(String message ,CodePoint p){
		this.message = message;
		this.point = p;
	}

	@Override
	public String getMessage(){
		return message;
	}
	
	@Override
	public String toString(){
		String ret = "";
		ret+="ErrorMarker: "+this.getMessage();
		return ret;
	}

	@Override
	public CodePoint getPoint() {
		return this.point;
	}

	@Override
	public boolean equals(Object eq){
		if(! (eq instanceof ErrorMarker))return false;
		ErrorMarker equals = (ErrorMarker) eq;
		if(!(equals.point.equals(this.point)))return false;
		if(!(equals.getMessage().equals(this.getMessage())))return false;
		
		return true;
	}

	@Override
	public void setAnnotation(Annotation addAnnotation) {
		// TODO Auto-generated method stub
		
	}

}
