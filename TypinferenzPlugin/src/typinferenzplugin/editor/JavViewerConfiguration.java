package typinferenzplugin.editor;

import java.util.ArrayList;
import java.util.Vector;

import org.eclipse.jface.text.DefaultIndentLineAutoEditStrategy;
import org.eclipse.jface.text.DefaultTextHover;
import org.eclipse.jface.text.IAutoEditStrategy;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextDoubleClickStrategy;
import org.eclipse.jface.text.ITextHover;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.contentassist.CompletionProposal;
import org.eclipse.jface.text.contentassist.ContentAssistant;
import org.eclipse.jface.text.contentassist.ContextInformation;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContentAssistant;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.jface.text.presentation.IPresentationReconciler;
import org.eclipse.jface.text.presentation.PresentationReconciler;
import org.eclipse.jface.text.quickassist.IQuickAssistAssistant;
import org.eclipse.jface.text.quickassist.IQuickAssistInvocationContext;
import org.eclipse.jface.text.quickassist.IQuickAssistProcessor;
import org.eclipse.jface.text.quickassist.QuickAssistAssistant;
import org.eclipse.jface.text.reconciler.IReconciler;
import org.eclipse.jface.text.reconciler.MonoReconciler;
import org.eclipse.jface.text.rules.BufferedRuleBasedScanner;
import org.eclipse.jface.text.rules.DefaultDamagerRepairer;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.DefaultAnnotationHover;
import org.eclipse.jface.text.source.IAnnotationHover;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.SourceViewerConfiguration;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;

import typinferenzplugin.TypeReplaceMarker;


public class JavViewerConfiguration extends SourceViewerConfiguration {


	private JavEditor editor;

	public JavViewerConfiguration(JavEditor parentEditor){
		super();
		this.editor = editor;
	}
	
	/**
	 * Single token scanner.
	 */
	static class SingleTokenScanner extends BufferedRuleBasedScanner {
		public SingleTokenScanner(TextAttribute attribute) {
			setDefaultReturnToken(new Token(attribute));
		}
	}

	@Override
	public IReconciler getReconciler(ISourceViewer sourceViewer) {
		// TODO Auto-generated method stub
		return new MonoReconciler(new AnnotationProvider(),true);
	}

	/* (non-Javadoc)
	 * Method declared on SourceViewerConfiguration
	 */
	public String[] getIndentPrefixes(ISourceViewer sourceViewer, String contentType) {
		return new String[] { "\t", "    " }; //$NON-NLS-1$ //$NON-NLS-2$
	}

/* (non-Javadoc)
 * Method declared on SourceViewerConfiguration
 
public IPresentationReconciler getPresentationReconciler(ISourceViewer sourceViewer) {

	JavaColorProvider provider= JavaEditorExamplePlugin.getDefault().getJavaColorProvider();
	PresentationReconciler reconciler= new PresentationReconciler();
	reconciler.setDocumentPartitioning(getConfiguredDocumentPartitioning(sourceViewer));

	DefaultDamagerRepairer dr= new DefaultDamagerRepairer(JavaEditorExamplePlugin.getDefault().getJavaCodeScanner());
	reconciler.setDamager(dr, IDocument.DEFAULT_CONTENT_TYPE);
	reconciler.setRepairer(dr, IDocument.DEFAULT_CONTENT_TYPE);

	dr= new DefaultDamagerRepairer(JavaEditorExamplePlugin.getDefault().getJavaDocScanner());
	reconciler.setDamager(dr, JavaPartitionScanner.JAVA_DOC);
	reconciler.setRepairer(dr, JavaPartitionScanner.JAVA_DOC);

	dr= new DefaultDamagerRepairer(new SingleTokenScanner(new TextAttribute(provider.getColor(JavaColorProvider.MULTI_LINE_COMMENT))));
	reconciler.setDamager(dr, JavaPartitionScanner.JAVA_MULTILINE_COMMENT);
	reconciler.setRepairer(dr, JavaPartitionScanner.JAVA_MULTILINE_COMMENT);

	return reconciler;
}
*/

	
	@Override
	public IAnnotationHover getAnnotationHover(ISourceViewer sourceViewer) {
		return new DefaultAnnotationHover();
	}
	
	/* (non-Javadoc)
	 * Method declared on SourceViewerConfiguration
	 */
	public int getTabWidth(ISourceViewer sourceViewer) {
		return 4;
	}
	
	/* (non-Javadoc)
	 * Method declared on SourceViewerConfiguration
	 */
	public ITextHover getTextHover(ISourceViewer sourceViewer, String contentType) {
		return new DefaultTextHover(sourceViewer);
	}
	
	
	@Override
	public IQuickAssistAssistant getQuickAssistAssistant(ISourceViewer sourceViewer) {
		//Beschreibung: https://stackoverflow.com/questions/8866688/eclipse-pde-custom-quickfix-only-available-in-problems-view
		QuickAssistAssistant ret =  new QuickAssistAssistant();
		ret.setQuickAssistProcessor(new QuickAssistProcessor(this.editor));
		ret.setInformationControlCreator(getInformationControlCreator(sourceViewer));
		return ret;
	}
	
}


class QuickAssistProcessor implements IQuickAssistProcessor{

	private JavEditor editor;

	public QuickAssistProcessor(JavEditor editor){
		this.editor = editor;
	}
	
	@Override
	public String getErrorMessage() {
		return "test";
	}

	@Override
	public boolean canFix(Annotation annotation) {
		return true;
	}

	@Override
	public boolean canAssist(IQuickAssistInvocationContext invocationContext) {
		return true;
	}

	@Override
	public ICompletionProposal[] computeQuickAssistProposals(
			IQuickAssistInvocationContext invocationContext) {
		// TODO Hier eine ICompletionProposal zurückliefern. Dadurch werden Marker "klickbar"
		ArrayList<ICompletionProposal> ret = new ArrayList<ICompletionProposal>();
		Vector<TypeReplaceMarker> markers = this.editor.getMarkersAt(invocationContext.getOffset());
		if(markers != null)for(TypeReplaceMarker m : markers){
			ret.add(new TypInsertCompletionProposal(m));
		}
		ret.add(new TypInsertCompletionProposal(null));
		return (ICompletionProposal[]) ret.toArray();
	}
	
}

class TypInsertCompletionProposal implements ICompletionProposal{

	private TypeReplaceMarker marker;

	public TypInsertCompletionProposal(TypeReplaceMarker m) {
		this.marker = m;
	}

	@Override
	public void apply(IDocument document) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Point getSelection(IDocument document) {
		return new Point(1, 1);
	}

	@Override
	public String getAdditionalProposalInfo() {
		return "test";
	}

	@Override
	public String getDisplayString() {
		return "Test";
	}

	@Override
	public Image getImage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IContextInformation getContextInformation() {
		// TODO Auto-generated method stub
		return new ContextInformation("test","test");
	}
	
}