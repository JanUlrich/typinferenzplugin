package typinferenzplugin.editor;


import java.util.Vector;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.views.contentoutline.ContentOutlinePage;

import de.dhbwstuttgart.syntaxtree.Field;
import de.dhbwstuttgart.syntaxtree.Method;
import de.dhbwstuttgart.syntaxtree.SourceFile;
import de.dhbwstuttgart.syntaxtree.SyntaxTreeNode;
import de.dhbwstuttgart.syntaxtree.statement.Block;
import de.dhbwstuttgart.syntaxtree.statement.LocalVarDecl;
import de.dhbwstuttgart.syntaxtree.visual.ASTPrinter;
import de.dhbwstuttgart.syntaxtree.visual.OutputGenerator;
import de.dhbwstuttgart.typedeployment.TypeInsert;
import typinferenzplugin.TypeReplaceMarker;
import typinferenzplugin.editor.treeView.SyntaxTreeOperations;

public class JavOutline extends ContentOutlinePage{
	//HOWTO Controls: http://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.platform.doc.isv%2Fguide%2Fswt_widgets_controls.htm&resultof=%22outline%22%20%22outlin%22%20%22control%22


	private SourceFile syntaxTree;
	private Vector<TypeReplaceMarker> markers;

	public JavOutline(SourceFile syntaxTree, Vector<TypeReplaceMarker> markers) {
		this.syntaxTree = syntaxTree;
		this.markers = markers;
	}

	/* (non-Javadoc)
	 * Method declared on ContentOutlinePage
	 */
	public void createControl(Composite parent) {

		super.createControl(parent);

		TreeViewer viewer = getTreeViewer();
		viewer.setContentProvider(new SyntaxTreeContentProvider(syntaxTree));
		viewer.setLabelProvider(new SyntaxTreeLabelProvider());
		viewer.addSelectionChangedListener(this);
		if(this.syntaxTree!=null)viewer.setInput(this.syntaxTree);

		//ContextMenu für die TreeView:
		final MenuManager menuMgr = new MenuManager();
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new FillContextMenu(viewer, this));
		final Control tree = viewer.getControl();
		final Menu menu = menuMgr.createContextMenu(tree);
		tree.setMenu(menu);
	}

	/* (non-Javadoc)
	 * Method declared on ContentOutlinePage
	 */
	public void selectionChanged(SelectionChangedEvent event) {

		super.selectionChanged(event);
		/*
		ISelection selection= event.getSelection();
		if (selection.isEmpty())
			fTextEditor.resetHighlightRange();
		else {
			Segment segment= (Segment) ((IStructuredSelection) selection).getFirstElement();
			int start= segment.position.getOffset();
			int length= segment.position.getLength();
			try {
				fTextEditor.setHighlightRange(start, length, true);
			} catch (IllegalArgumentException x) {
				fTextEditor.resetHighlightRange();
			}
		}
		*/
	}

	public void update(SourceFile sourceFile, Vector<TypeReplaceMarker> markers) {
		this.markers = markers;
		TreeViewer viewer= getTreeViewer();

		if (viewer != null) {
			Control control= viewer.getControl();
			if (control != null && !control.isDisposed()) {
				control.setRedraw(false);
				viewer.setInput(sourceFile);
				viewer.expandAll();
				control.setRedraw(true);
			}
		}
	}

	public Vector<TypeReplaceMarker> getMarkers() {
		return this.markers;
	}

}

class FillContextMenu implements IMenuListener
{
	private TreeViewer tree;
	private JavOutline outline;

	public FillContextMenu(TreeViewer forTree, JavOutline outline){
		this.tree = forTree;
		this.outline = outline;
	}

	@Override
	public void menuAboutToShow(IMenuManager manager) {
		//Der SyntaxTreeNode der angeklickt wurde:
		//TODO: Im Voraus HashMap mit den Syntaxtreenodes und den zugehörigen TypeInserts berechnen.
		//Diese dann bei Gelegenheit anzeigen
		Object selectedObject = ((ITreeSelection)this.tree.getSelection()).getFirstElement();
		if(selectedObject instanceof Field){
			Field node = (Field) selectedObject;

			if(this.outline != null)for(TypeReplaceMarker marker : this.outline.getMarkers()){
				TypeInsert point = marker.getInsertPoint();
				if(marker.getPoint().offset.equals(node.getType().getOffset())){
					String label = point.getInsertString()+" einsetzen";
					manager.add(new InsertTypeAction(label, marker));
				}
			}
		}
		if(selectedObject instanceof Method){
			Method node = (Method) selectedObject;

			if(this.outline != null)for(TypeReplaceMarker marker : this.outline.getMarkers()){
				TypeInsert point = marker.getInsertPoint();
				if(marker.getPoint().offset.equals(node.getReturnType().getOffset())){
					String label = point.getInsertString()+" einsetzen";
					manager.add(new InsertTypeAction(label, marker));
				}
			}
		}
	}
}

class InsertTypeAction extends Action{

	private String text;
	private TypeReplaceMarker marker;

	public InsertTypeAction(String text, TypeReplaceMarker marker){
		this.text = text;
		this.marker = marker;
	}

	@Override
	public String getDescription() {
		return this.text;
	}

	@Override
	public String getText() {
		return this.text;
	}

	@Override
	public void run() {
		super.run();
		marker.run();
	}

}

class SyntaxTreeContentProvider implements ITreeContentProvider {

	private SourceFile ast;

	public SyntaxTreeContentProvider(SourceFile ast) {
		this.ast = ast;
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
	}

	@Override
	public Object[] getElements(Object inputElement) {
		if(inputElement instanceof SyntaxTreeNode)
			return SyntaxTreeOperations.getChildren((SyntaxTreeNode)inputElement).toArray();
		else return null;
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		return getElements(parentElement);
	}

	@Override
	public Object getParent(Object element) {
		if(! (element instanceof SyntaxTreeNode))return null;
		return SyntaxTreeOperations.getParent((SyntaxTreeNode)element, ast);
	}

	@Override
	public boolean hasChildren(Object element) {
		if(! (element instanceof SyntaxTreeNode))return false;
		return this.getChildren(element).length>0;
	}
}

class SyntaxTreeLabelProvider extends LabelProvider{

	@Override
	public String getText(Object element) {
		if(! (element instanceof SyntaxTreeNode))return super.getText(element);
		TreeNodeDescriptionGenerator generator = new TreeNodeDescriptionGenerator(((SyntaxTreeNode)element));
		return generator.getDescription();
		//return "test";
	}

}
