package typinferenzplugin.editor.treeView;

import java.util.ArrayList;
import java.util.List;

import de.dhbwstuttgart.parser.SyntaxTreeGenerator.AssignToLocal;
import de.dhbwstuttgart.syntaxtree.ASTVisitor;
import de.dhbwstuttgart.syntaxtree.ClassOrInterface;
import de.dhbwstuttgart.syntaxtree.Constructor;
import de.dhbwstuttgart.syntaxtree.Field;
import de.dhbwstuttgart.syntaxtree.FormalParameter;
import de.dhbwstuttgart.syntaxtree.GenericDeclarationList;
import de.dhbwstuttgart.syntaxtree.GenericTypeVar;
import de.dhbwstuttgart.syntaxtree.Method;
import de.dhbwstuttgart.syntaxtree.ParameterList;
import de.dhbwstuttgart.syntaxtree.SourceFile;
import de.dhbwstuttgart.syntaxtree.SyntaxTreeNode;
import de.dhbwstuttgart.syntaxtree.statement.ArgumentList;
import de.dhbwstuttgart.syntaxtree.statement.Assign;
import de.dhbwstuttgart.syntaxtree.statement.AssignToField;
import de.dhbwstuttgart.syntaxtree.statement.BinaryExpr;
import de.dhbwstuttgart.syntaxtree.statement.Block;
import de.dhbwstuttgart.syntaxtree.statement.CastExpr;
import de.dhbwstuttgart.syntaxtree.statement.DoStmt;
import de.dhbwstuttgart.syntaxtree.statement.EmptyStmt;
import de.dhbwstuttgart.syntaxtree.statement.ExpressionReceiver;
import de.dhbwstuttgart.syntaxtree.statement.FieldVar;
import de.dhbwstuttgart.syntaxtree.statement.ForStmt;
import de.dhbwstuttgart.syntaxtree.statement.IfStmt;
import de.dhbwstuttgart.syntaxtree.statement.InstanceOf;
import de.dhbwstuttgart.syntaxtree.statement.LambdaExpression;
import de.dhbwstuttgart.syntaxtree.statement.Literal;
import de.dhbwstuttgart.syntaxtree.statement.LocalVar;
import de.dhbwstuttgart.syntaxtree.statement.LocalVarDecl;
import de.dhbwstuttgart.syntaxtree.statement.MethodCall;
import de.dhbwstuttgart.syntaxtree.statement.NewArray;
import de.dhbwstuttgart.syntaxtree.statement.NewClass;
import de.dhbwstuttgart.syntaxtree.statement.Receiver;
import de.dhbwstuttgart.syntaxtree.statement.Return;
import de.dhbwstuttgart.syntaxtree.statement.ReturnVoid;
import de.dhbwstuttgart.syntaxtree.statement.Statement;
import de.dhbwstuttgart.syntaxtree.statement.StaticClassName;
import de.dhbwstuttgart.syntaxtree.statement.Super;
import de.dhbwstuttgart.syntaxtree.statement.SuperCall;
import de.dhbwstuttgart.syntaxtree.statement.This;
import de.dhbwstuttgart.syntaxtree.statement.UnaryExpr;
import de.dhbwstuttgart.syntaxtree.statement.WhileStmt;
import de.dhbwstuttgart.syntaxtree.type.ExtendsWildcardType;
import de.dhbwstuttgart.syntaxtree.type.GenericRefType;
import de.dhbwstuttgart.syntaxtree.type.RefType;
import de.dhbwstuttgart.syntaxtree.type.SuperWildcardType;
import de.dhbwstuttgart.syntaxtree.type.TypePlaceholder;

public class ASTChildren implements ASTVisitor{

	private final List<SyntaxTreeNode> children = new ArrayList<>();
	
	public ASTChildren(SyntaxTreeNode ofNode) {
		ofNode.accept(this);
	}
	
	public List<SyntaxTreeNode> getChildren(){
		return children;
	}
	
	@Override
	public void visit(LambdaExpression lambdaExpression) {
		children.add(lambdaExpression.params);
		children.add(lambdaExpression.methodBody);
	}

	@Override
	public void visit(Assign assign) {
		children.add(assign.lefSide);
		children.add(assign.rightSide);
	}

	@Override
	public void visit(BinaryExpr binary) {
	}

	@Override
	public void visit(Block block) {
		for(Statement s : block.statements) {
			children.add(s);
		}
	}

	@Override
	public void visit(CastExpr castExpr) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(EmptyStmt emptyStmt) {
	}

	@Override
	public void visit(FieldVar fieldVar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ForStmt forStmt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IfStmt ifStmt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(InstanceOf instanceOf) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(LocalVar localVar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(LocalVarDecl localVarDecl) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(MethodCall methodCall) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(NewClass methodCall) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(NewArray newArray) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Return aReturn) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ReturnVoid aReturn) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(StaticClassName staticClassName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Super aSuper) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(This aThis) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void visit(WhileStmt whileStmt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(SourceFile sourceFile) {
		for(ClassOrInterface cl : sourceFile.getClasses()) {
			children.add(cl);
		}
		
	}

	@Override
	public void visit(ArgumentList argumentList) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(GenericTypeVar genericTypeVar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(FormalParameter formalParameter) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(GenericDeclarationList genericTypeVars) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Field field) {
	}

	@Override
	public void visit(Method field) {
		children.add(field.getParameterList());
		//children.add(field.block);
	}

	@Override
	public void visit(ParameterList formalParameters) {
		for(FormalParameter fp : formalParameters.getFormalparalist()) {
			children.add(fp);
		}
	}

	@Override
	public void visit(ClassOrInterface classOrInterface) {
		for(Field f : classOrInterface.getFieldDecl()) {
			children.add(f);
		}
		for(Method f : classOrInterface.getMethods()) {
			children.add(f);
		}
	}

	@Override
	public void visit(RefType refType) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(SuperWildcardType superWildcardType) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(TypePlaceholder typePlaceholder) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ExtendsWildcardType extendsWildcardType) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(GenericRefType genericRefType) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(DoStmt whileStmt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(AssignToField assignLeftSide) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(AssignToLocal assignLeftSide) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(SuperCall superCall) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Constructor field) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ExpressionReceiver arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void visit(UnaryExpr arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Literal arg0) {
		// TODO Auto-generated method stub
		
	}

}
