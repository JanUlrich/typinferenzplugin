package typinferenzplugin.editor.treeView;

import java.util.List;

import de.dhbwstuttgart.syntaxtree.SourceFile;
import de.dhbwstuttgart.syntaxtree.SyntaxTreeNode;

public class SyntaxTreeOperations {
	public static List<SyntaxTreeNode> getChildren(SyntaxTreeNode ofNode){
		return new ASTChildren(ofNode).getChildren();
	}

	public static SyntaxTreeNode getParent(SyntaxTreeNode element, SourceFile syntaxTree) {
		SyntaxTreeNode ret = getParentRecursive(element, syntaxTree);
		return ret;
		
	}

	private static SyntaxTreeNode getParentRecursive(SyntaxTreeNode forElement, SyntaxTreeNode possibleParent) {
		List<SyntaxTreeNode> children = getChildren(possibleParent);
		if(children.contains(forElement))return possibleParent;
		else {
			if(children.size() == 0)return null;
			for(SyntaxTreeNode child : children) {
				SyntaxTreeNode parent = getParentRecursive(forElement, child);
				if(parent != null)return parent;
			}
			return null;
		}
	}
}
