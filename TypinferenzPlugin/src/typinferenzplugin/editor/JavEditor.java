package typinferenzplugin.editor;

import java.util.HashMap;

import typinferenzplugin.*;

import java.util.Vector;

import org.eclipse.core.internal.resources.Marker;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.texteditor.ResourceMarkerAnnotationModel;
import org.eclipse.ui.texteditor.SimpleMarkerAnnotation;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;

import de.dhbwstuttgart.exceptions.TypeinferenceException;
import de.dhbwstuttgart.syntaxtree.SourceFile;
import typinferenzplugin.TypeReplaceMarker;
import typinferenzplugin.Typinferenz;
import typinferenzplugin.error.ErrorOutput;

//Example from: http://help.eclipse.org/indigo/index.jsp
/**
 * Editor für .jav-Dateien
 * Anmerkung: Für jede geöffnete Datei wird eine Instanz des Editors erstellt
 * @author janulrich
 *
 */
public class JavEditor extends TextEditor{
	private Typinferenz typeinference;
	
	private JavOutline outlinePage;
	/**
	 * Der SyntaxBaum für das aktuell geöffnete Dokument.
	 */
	private SourceFile sourceFile;
	
	/**
	 * Die TypeReplaceMarker für das aktuell geöffnete Dokument
	 */
	private Vector<JavMarker> errorMarkers = new Vector<JavMarker>();

	private Vector<TypeReplaceMarker> typeReplaceMarkers = new Vector<TypeReplaceMarker>();

	//Help: http://wiki.eclipse.org/FAQ_How_do_I_get_started_with_creating_a_custom_text_editor%3F
	public JavEditor(){
		super();
		System.out.println("TEST");
		//install the source configuration
		//setSourceViewerConfiguration(new HTMLConfiguration());
		//install the document provider
		//setDocumentProvider(new HTMLDocumentProvider());
		typeinference = new Typinferenz(this);
	}

	
	
	@Override
	protected void doSetInput(IEditorInput input) throws CoreException {
		super.doSetInput(input);
		this.typeReconstruction(); // Marker generieren
	}



	/* (non-Javadoc)
	 * Method declared on AbstractTextEditor
	 */
	protected void initializeEditor() {
		super.initializeEditor();
		setSourceViewerConfiguration(new JavViewerConfiguration(this));
		//install the document provider
		//setDocumentProvider(new JavFileProvider());
	}
	
	@Override
	public void doSave(IProgressMonitor monitor) {
		super.doSave(monitor);
        //Wird aufgerufen, sobald das Dokument gespeichert wird.
		this.removeMarkers();
		this.typeReconstruction();
	}

	/**
	 * Startet den Typinferenzalgorithmus, zuvor sollten alle Marker entfernt werden
	 */
	private void typeReconstruction() {
		Vector<JavMarker> markers= new Vector<JavMarker>();
		if(!this.typeReplaceMarkers.isEmpty() || !this.errorMarkers.isEmpty()){
			new ErrorOutput("Fehler: Zuerst Marker löschen, bevor Typinferenz durchgeführt werden kann");
			return;
		}
		//this.typeReplaceMarkers = new Vector<TypeReplaceMarker>();
		//this.errorMarkers = new Vector<JavMarker>();
		try{
			this.typeReplaceMarkers = typeinference.run();
			for(TypeReplaceMarker m : this.getTypeReplaceMarkers()){
				markers.add(m);
			};	
		}catch(TypeinferenceException texc){
			markers.add(new ErrorMarker(texc.getMessage(),new CodePoint(texc.getOffset())));
		}
		//Anschließend die TypeReplaceMarker im Quellcode anzeigen: https://stackoverflow.com/questions/8945371/how-to-implement-quick-fix-quick-assist-for-custom-eclipse-editor
		System.out.println("Typinferenz durchgeführt. Berechnete Marker:\n"+markers);
		
		IResource activeDocument = extractResource();
		if(activeDocument == null){
			new ErrorOutput("Kann das aktive Dokument nicht ermitteln");
		}else{
			this.placeMarkers(activeDocument, markers);
		}
		this.sourceFile = typeinference.getSourceFile();
		this.updateOutlinePage();
		//this.outlinePage.update(this.sourceFile,this.markers);
	}



	IResource extractResource() {
      IEditorInput input = this.getEditorInput();
      if (!(input instanceof IFileEditorInput))
         return null;
      return ((IFileEditorInput)input).getFile();
   }
	
	private void placeMarkers(IResource inDocument, Vector<? extends JavMarker> markers){
		//Marker Tut: http://wiki.eclipse.org/FAQ_How_do_I_create_my_own_tasks,_problems,_bookmarks,_and_so_on%3F
		//ResourceMarkerAnnotationModel model = new ResourceMarkerAnnotationModel(inDocument.); 
		for(JavMarker rm : markers){
			CodePoint point = rm.getPoint();
			try {
				IMarker m = inDocument.createMarker(Activator.TYPINFERENZMARKER_NAME);
				m.setAttribute(IMarker.CHAR_START, point.getPositionInCode());
				m.setAttribute(IMarker.CHAR_END, point.getPositionInCode()+1);
				String message = rm.getMessage();
				m.setAttribute(IMarker.MESSAGE, message);
				m.setAttribute(IMarker.SEVERITY, 1);
				rm.setAnnotation(this.addAnnotation(m, point.getPositionInCode()));
			} catch (CoreException e) {
				new ErrorOutput("Fehler beim Generieren eines Markers");
				e.printStackTrace();
			}
		}
	}
	
	private Annotation addAnnotation(IMarker marker, int offset) {
		//The DocumentProvider enables to get the document currently loaded in the editor
		IDocumentProvider idp = this.getDocumentProvider();
		
		//This is the document we want to connect to. This is taken from 
		//the current editor input.
		IDocument document = idp.getDocument(this.getEditorInput());
		
		//The IannotationModel enables to add/remove/change annotation to a Document 
		//loaded in an Editor
		IAnnotationModel iamf = idp.getAnnotationModel(this.getEditorInput());
		
		//Note: The annotation type id specify that you want to create one of your 
		//annotations
		SimpleMarkerAnnotation ma = new SimpleMarkerAnnotation("typinferenzplugin.annotation",marker);
		
		//Finally add the new annotation to the model
		iamf.connect(document);
		iamf.addAnnotation(ma,new Position(offset,1));
		iamf.disconnect(document);
		return ma;
	}
	
	/**
	 * Löscht die zu marker gehörende Annotation
	 * @param marker
	 */
	private void deleteAnnotation(JavMarker marker){
		IDocumentProvider idp = this.getDocumentProvider();
		IDocument document = idp.getDocument(this.getEditorInput());
		IAnnotationModel iamf = idp.getAnnotationModel(this.getEditorInput());
		
		iamf.connect(document);
		iamf.removeAnnotation(marker.getAnnotation());
		iamf.disconnect(document);
	}
	
	/**
	 * Erzeugt die Outline-View
	 */
	public Object getAdapter(Class required) {
		if (IContentOutlinePage.class.equals(required)) {
			if (outlinePage == null) {
				outlinePage= new JavOutline(this.sourceFile, this.getTypeReplaceMarkers());
			}
			this.updateOutlinePage();
			return outlinePage;
		}
		return super.getAdapter(required);
	}


	/**
	 * Diese Funktion führt einen ReplaceMarker aus.
	 * @param typeReplaceMarker
	 */
	public void runReplaceMarker(TypeReplaceMarker typeReplaceMarker) {
		IDocument document = this.getDocumentProvider().getDocument(this.getEditorInput());
		document.set(typeReplaceMarker.insertType(document.get()));
		this.removeMarkers();
		//Erneut den Typinferenzalgorithmus ausführen:
		this.typeReconstruction();
	}
	
	private void removeMarkers(){
		IResource document = this.extractResource();
		try {
			document.deleteMarkers(Activator.TYPINFERENZMARKER_NAME, true, IProject.DEPTH_INFINITE);
		} catch (CoreException e) {
			e.printStackTrace();
		}
		for(JavMarker jm : errorMarkers)this.deleteAnnotation(jm);
		for(JavMarker jm : typeReplaceMarkers)this.deleteAnnotation(jm);
		this.errorMarkers.removeAllElements();
		this.typeReplaceMarkers.removeAllElements();
		this.updateOutlinePage();
		//this.outlinePage.update(sourceFile, markers);
	}

	/**
	 * Aktualisiert die OutlinePage, falls vorhanden.
	 * Muss nach Änderungen an markers aufgerufen werden.
	 */
	private void updateOutlinePage() {
		if(this.outlinePage!= null && this.sourceFile != null && this.errorMarkers != null)
			this.outlinePage.update(sourceFile, this.getTypeReplaceMarkers());
	}

	public String getSourceCode() {
		return this.getDocumentProvider().getDocument(this.getEditorInput()).get();
	}
	
	private Vector<TypeReplaceMarker> getTypeReplaceMarkers(){
		return this.typeReplaceMarkers ;
	}
	
	/**
	 * Ermittelt die momentan an dem Dokument, für welches dieser Editor zuständig ist, angebrachten TypeReplaceMarker.
	 * Dabei werden die ReplaceMarker herausgefiltert, welche mindestens einen Typ an dem übergebenen offset einsetzen.
	 * @param offset
	 * @return
	 */
	public Vector<TypeReplaceMarker> getMarkersAt(int offset){
		return null;
	}

	public IPath getFilePath() {
		return ((FileEditorInput) this.getEditorInput()).getPath();
	}
	
}
