package typinferenzplugin;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.eclipse.jface.text.IDocument;

import de.dhbwstuttgart.bytecode.BytecodeGen;
import de.dhbwstuttgart.core.JavaTXCompiler;
import de.dhbwstuttgart.exceptions.*;
import de.dhbwstuttgart.syntaxtree.SourceFile;
import de.dhbwstuttgart.typedeployment.TypeInsert;
import de.dhbwstuttgart.typedeployment.TypeInsertFactory;
import de.dhbwstuttgart.typeinference.result.ResultSet;
import typinferenzplugin.editor.JavEditor;
import typinferenzplugin.error.ErrorOutput;
import java.util.*;

public class Typinferenz {

	private JavEditor editor;
	private SourceFile parsedSource;
	
	public Typinferenz(JavEditor forEditor)
	{
		this.editor = forEditor;
	}
	
	public Vector<TypeReplaceMarker> run() throws TypeinferenceException{
		Vector<TypeReplaceMarker> ret = new Vector<TypeReplaceMarker>();
		try{
		JavaTXCompiler compiler = new JavaTXCompiler(Arrays.asList(editor.getFilePath().toFile()));
		this.parsedSource = compiler.sourceFiles.get(editor.getFilePath().toFile());
		List<ResultSet> tiResults = compiler.typeInference();
		Set<TypeInsert> tips  = new HashSet<>();
		for(ResultSet tiResult : tiResults) {
			tips.addAll(TypeInsertFactory.createTypeInsertPoints(parsedSource, tiResult));
			int numberOfSolutions = 0; //Zählt mit, wie viele verschiedene Lösungen angeboten werden
			for(TypeInsert p : tips){
				TypeReplaceMarker toAdd = new TypeReplaceMarker(editor,p);
				if(!ret.contains(toAdd)){
					ret.add(toAdd);
				}
				else{
					System.out.println("Marker bereits vorhanden: "+toAdd.toString());		
				}
			}
		}
		/*
		//Bytecode generieren:
		String outputDirectory = editor.getFilePath().toString();
		outputDirectory = outputDirectory.substring(0, outputDirectory.length()-editor.getFilePath().lastSegment().length());//".jav" hat länge 4
		List<ByteCodeResult> bytecodeResults = compiler.generateBytecode(parsedSourceFiles, new TypeinferenceResults(results));
		for(ByteCodeResult result: bytecodeResults){				
			JavaClass javaClass = result.getByteCode().getJavaClass();
			javaClass.dump(new File(outputDirectory+javaClass.getClassName()+".class"));
			
			for(ClassGenerator cg: result.getByteCode().getExtraClasses().values()){
				JavaClass jc = cg.getJavaClass();
				jc.dump(new File(outputDirectory+jc.getClassName()+".class"));
			}
		}
		*/
		if(tips.size() == 0 && tiResults.size()>0) {
	        for(SourceFile sf : compiler.sourceFiles.values()){
	        	HashMap<String,byte[]> bytecode = getBytecode(sf, tiResults.get(0));
	        	this.writeClassFile(bytecode);
	        }
	    }
		}catch(TypeinferenceException texc){
			throw texc; //Die aufrufende Instanz sollte daraus dann einen ErrorMarker generieren.
		}catch(Exception exc){
			exc.printStackTrace(); //Hier wird kein Marker generiert... nur Debug-Output
			new ErrorOutput("Fehler beim Parsen und Inferieren");
		}
		
		return ret;
	}


    public HashMap<String,byte[]> getBytecode(SourceFile sf, ResultSet resultSet) {
    	/*
    		HashMap<String,byte[]> classFiles = new HashMap<>();
    		BytecodeGen bytecodeGen = new BytecodeGen(classFiles,resultSet);
    		bytecodeGen.visit(sf);
    		return bytecodeGen.getClassFiles();
    		*/
    	throw new NotImplementedException(); //Fayez fragen
	}
    
    public void writeClassFile(HashMap<String,byte[]> classFiles) {
    		FileOutputStream output;
    		for(String name : classFiles.keySet()) {
    			byte[] bytecode = classFiles.get(name);
    			try {
    				System.out.println("generating"+name+ ".class file");
    				output = new FileOutputStream(new File(System.getProperty("user.dir") + "/testBytecode/generatedBC/" +name+".class"));
    				output.write(bytecode);
    				output.close();
    				System.out.println(name+".class file generated");
    			} catch (FileNotFoundException e) {
    				e.printStackTrace();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		}
    		
    }
	
	public SourceFile getSourceFile() {
		return parsedSource;
	}
}
