package typinferenzplugin;

import java.util.Vector;

import org.eclipse.jface.text.source.Annotation;

public abstract class JavMarker {

	private Annotation annotation;

	public abstract CodePoint getPoint();
	public abstract String getMessage();
	
	public void setAnnotation(Annotation addAnnotation) {
		this.annotation = addAnnotation;
	}

	public Annotation getAnnotation() {
		return annotation;
	}
	
}
