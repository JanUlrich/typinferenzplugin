package typinferenzplugin.marker;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.IMarkerResolution;
import org.eclipse.ui.IMarkerResolutionGenerator;

public class QuickFixer implements IMarkerResolutionGenerator {
	
      public IMarkerResolution[] getResolutions(IMarker mk) {
         return new IMarkerResolution[] {
		   new QuickFix("Fix #1 for ")
		};
      }


}
