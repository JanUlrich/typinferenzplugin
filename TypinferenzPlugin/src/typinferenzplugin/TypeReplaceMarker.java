package typinferenzplugin;

import de.dhbwstuttgart.typedeployment.TypeInsert;

import typinferenzplugin.editor.JavEditor;

public class TypeReplaceMarker extends JavMarker{

	private JavEditor editor;
	private CodePoint point;
	private TypeInsert tip;

	public TypeReplaceMarker(JavEditor editor, TypeInsert point){
		this.editor = editor;
		this.point = new CodePoint(point.point.point);
		this.tip = point;
	}

	public void run(){
		editor.runReplaceMarker(this);
	}

	public TypeInsert getInsertPoint() {
		return tip;
	}

	@Override
	public String toString(){
		String ret = "";
		ret+="TypeReplaceMarker: "+tip.point;
		/*
		for(TypeInsertPoint point : this.points.points){
			ret+=point.getTypeInsertString()+", ";
		}
		*/
		return ret;
	}

	@Override
	public CodePoint getPoint() {
		return point;
	}

	@Override
	public String getMessage() {
		String ret = "";
		/*
		Iterator<TypeInsertPoint> it = this.points.points.iterator();
		while(it.hasNext()){
			ret+=it.next().getTypeInsertString();
			if(it.hasNext())ret+=", ";
		}
		*/
		ret+=this.tip.getInsertString()+" einsetzen";
		return ret;
	}

	public String insertType(String fileContent) {
		return this.tip.insert(fileContent);
	}

}
