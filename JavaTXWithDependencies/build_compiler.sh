#!/bin/bash
# A basic script to compile the necessary packages and their subpackages to work with the parser.
# Messages are logged to stderr.
>&2 echo "Building de.dhbwstuttgart.typecheck..."
javac -d ../bin ./de/dhbwstuttgart/typecheck/*.java
>&2 echo "Building de.dhbwstuttgart.syntaxtree..."
javac -d ../bin ./de/dhbwstuttgart/syntaxtree/*.java
>&2 echo "Building de.dhbwstuttgart.syntaxtree.factory..."
javac -d ../bin ./de/dhbwstuttgart/syntaxtree/factory/*.java
>&2 echo "Building de.dhbwstuttgart.syntaxtree.operator..."
javac -d ../bin ./de/dhbwstuttgart/syntaxtree/operator/*.java
>&2 echo "Building de.dhbwstuttgart.syntaxtree.statement..."
javac -d ../bin ./de/dhbwstuttgart/syntaxtree/statement/*.java
>&2 echo "Building de.dhbwstuttgart.syntaxtree.statement.literal..."
javac -d ../bin ./de/dhbwstuttgart/syntaxtree/statement/literal/*.java
>&2 echo "Building de.dhbwstuttgart.syntaxtree.type..."
javac -d ../bin ./de/dhbwstuttgart/syntaxtree/type/*.java
>&2 echo "Building de.dhbwstuttgart.parser..."
javac -d ../bin ./de/dhbwstuttgart/parser/*.java
>&2 echo "Building de.dhbwstuttgart.parser.SyntaxTreeGenerator..."
javac -d ../bin ./de/dhbwstuttgart/parser/SyntaxTreeGenerator/*.java
>&2 echo "Building de.dhbwstuttgart.parser.antlr..."
javac -d ../bin ./de/dhbwstuttgart/parser/antlr/*.java
echo "Done. Now its your turn to debug:)."
